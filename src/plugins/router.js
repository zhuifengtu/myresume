import Vue from 'vue'
import VueRouter from 'vue-router'
import Resume from '../components/Resume'
import ResumeEn from '../components/ResumeEn'

const routes = [
    {
        path: '/',
        name: 'root',
        redirect: '/zh-CN',
        meta: {}
    },
    {
        path: '/zh-CN',
        name: 'US',
        component: Resume,
        meta: {}
    },
    {
        path: '/en-US',
        name: 'US',
        component: ResumeEn,
        meta: {}
    }
]

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: routes
})